from Constants import *

# a method to convert a string to a binary list
# str -> string to be converted, c -> choice (true = input, false = s-box)
def strTobin (str,c):
    if c:
        binary = list(bin(int(str, 16))[2:])                                    # list of strings [2:] -> to remove (ob from the start of string)
        if len(binary) < 64:
            for i in range(64 - len(binary)):
                binary.insert(0, 0)
    else:
        binary = binary = list(bin(int(str, 10))[2:])
        if len(binary) < 4:
            for i in range(4 - len(binary)):
                binary.insert(0, 0)
    return binary

# a method to convert a binary list to a string
def listTostr(list):
    res = ""
    for i in list:
        res += str(i)
    return res

# a method to perform bitwise xor-ing between 2 strings
# c -> true = 1st xor in round (48 -bits), false = 2nd xor in round
def Xor(str1, str2, c):
    ans = ""
    for i in range(len(str1)):
        if str1[i] == str2[i]:
            ans = ans + "0"
        else:
            ans = ans + "1"

    ans = list(ans)
    if c:
        if len(ans) < 48:
            for i in range(48 - len(ans)):
                ans.insert(0, 0)
    else:
        if len(ans) < 32:
            for i in range(32 - len(ans)):
                ans.insert(0, 0)
    return ans

# a method to perform permutation depending on what list is a given as a parameter
# p1 -> perfrom permutation choice 1 ...etc
# lis -> key or text, p -> permutation list, res -> result from permutation
def permute(lis, p):
    res = list()
    for x in p:
        res.append(lis[x - 1])
    return res

# a method to perform left circular shift based on round number
# key -> 28 bits of a key (list) round -> round number
def leftCircularShift(key, round):
    return key[LCS[round]:] + key[:LCS[round]]

# a method to generate key
# key -> 64 bits key, keys -> list of 48 bits keys for each round
def generateKeys(key):
    keys = list()

    # permutation choice 1
    key = permute(key, p1)                                                          # key is now 56 bits long
    left = key[0:28]                                                                # left 28 bits chunk of key
    right = key[28:56]                                                              # right 28 bits chunk of key

    # 16 rounds
    for i in range(16):
        left = leftCircularShift(left, i)
        right = leftCircularShift(right, i)

        # concat right chunk to the left chunk
        temp = left.copy()
        temp.extend(right)

        # permutation choice 2 and appending to keys
        keys.append(permute(temp, p2))                                              # 48 bits key
    return keys

# a method to perform s-box permutation
# text -> a 48 bits plaintext res -> 32 bits
def S_Box(text):
    chunks = list()
    res = list()

    # divide into chunks of 8 bits
    x = 0
    for i in range(0, 48, 6):
        chunks.append(text[i: i+6])
        x += 1

    j = 0
    for c in chunks:
        row = str(str(c[0]) + str(c[5]))
        row = int(row, 2)

        column = c[1:5]
        column = listTostr(column)
        column = int(column, 2)

        temp = strTobin(str(s[j][row][column]), False)
        res.extend(temp)
        j += 1
    return res

# a method to simulate a single round in des
# lis -> a binary list of plaintext, key -> key of this round, index -> index of round
def Round(left, right, key, index):
    # expansion permutation
    expand = permute(right, EP)

    # xor with round key
    xor = Xor(listTostr(expand), listTostr(key), True)

    # substitution box
    s = S_Box(xor)

    # permutation
    p = permute(s, PT)

    # xor
    result = Xor(listTostr(left), listTostr(p), False)
    left = result

    if index != 15:
        left, right = right, left

    return left, right


# a method to perform DES encryption
# txt -> plaintext/cipher text from user, key -> key from user, n -> number of times to perform encryption
# c -> choice whether  to reverse keys for decryption or not (True = encrypt, False = decrypt)
def DES_encrypt(pt, key, n, c):
    for j in range(n):
        # generate keys
        keys = generateKeys(key)

        if c == False:
            keys.reverse()                                                          # if we are decrypting all we need to do is to reverse the keys list

        # perform initial permutation
        pt = permute(pt, IP)

        # split plaintext
        left = pt[0:32]
        right = pt[32:]

        # 16 rounds
        for i in range(16):
            left, right = Round(left, right, keys[i], i)

        # perform inverse permutation
        temp = left.copy()
        temp.extend(right)
        result = permute(temp, IIP)
        pt = result
        result = listTostr(result)
        result = hex(int(result, 2))
        if j == n-1:
            return result