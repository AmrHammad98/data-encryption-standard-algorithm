from DES import *

if __name__ == '__main__':
    #  Key input from user
    key1 = input("Please enter 16 hexadecimal Character for Key\n")
    key1 = strTobin(key1, True)

    # PlainText input from user
    ptx = input("Please enter 16 hexadecimal Character for PlainText\n")
    ptx = strTobin(ptx, True)

    n = input("Please enter number of times you wish the encryption runs\n")
    n = int(n, 10)

    # encrypt
    print("Output\n")
    cipher = DES_encrypt(ptx, key1, n, True)
    print(cipher)

    # decrypt
    #  Key input from user
    key2 = input("Please enter 16 hexadecimal Character for Key\n")
    key2 = strTobin(key2, True)

    # PlainText input from user
    ctx = input("Please enter 16 hexadecimal Character for Ciphertext\n")
    ctx = strTobin(ctx, True)

    n = input("Please enter number of times you wish the decryption runs\n")
    n = int(n, 10)

    print("Output\n")
    plain = DES_encrypt(ctx, key1, n, False)
    print(plain)